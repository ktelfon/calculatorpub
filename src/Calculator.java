import java.util.Scanner;

public class Calculator {

    private Scanner scan;

    public Calculator(String type) {
        this.scan = new Scanner(System.in);
    }

    public void start() {
        System.out.println("Hello I'm a calculator");
        while (true) {
            int firstNumber = getNumberFromUser("Enter first number:");
            int secondNumber = getNumberFromUser("Enter second number:");
            String op = getOperationFromUser();
            int result = getResult(firstNumber, secondNumber, op);
            print(firstNumber, secondNumber, op, result);

        }
    }

    private void print(int firstNumber, int secondNumber, String op, int result) {
        System.out.println(firstNumber + " " + op + " " + secondNumber);
        System.out.println("Result: " + result);
    }

    private int getResult(int firstNumber, int secondNumber, String op) {
        switch (op) {
            case "+":
                return firstNumber + secondNumber;
            case "-":
                return firstNumber - secondNumber;
            case "*":
                return firstNumber * secondNumber;
            case "/":
                return firstNumber / secondNumber;
        }
        System.out.println("Operation not found!");
        return 0;
    }

    private String getOperationFromUser() {
        // fix nextInt()
        scan.nextLine();

        System.out.println("What to do?(+,-,*,/)");
        return scan.nextLine();
    }

    private int getNumberFromUser(String s) {
        System.out.println(s);
        return scan.nextInt();
    }
}
